/* Ответы на теоретические вопросы:
1. Метод — это функция закрепленная за объектом.
2. function может иметь свойства объекта.
3. Объект - ссылочный тип данных, так как переменные в нем фактически его значения не содержат, 
а содержат ссылку на место в памяти, где есть реальные данные.  */

function createNewUser() {
    let firstName = prompt('Введите имя');
    let lastName = prompt('Введите фамилию');
    const newUser = {
        firstName,
        lastName,
        getLogin() {
            return firstName.toLowerCase()[0] + lastName.toLowerCase()
        }
    }
    Object.defineProperties(newUser, {
        firstName: { writable: false },
        lastName: { writable: false },
    })
    return newUser
}
let user = createNewUser()
console.log(user.getLogin());

